// Copyright 2020 Derek Fletcher. All Rights Reserved.

#include "FPSGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FPSGame, "FPSGame" );
 