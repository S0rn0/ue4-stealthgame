// Copyright 2020 Derek Fletcher. All Rights Reserved.


#include "Launchpad.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "FPSCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ALaunchpad::ALaunchpad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

  MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;

  BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
  BoxComp->OnComponentBeginOverlap.AddDynamic(this, &ALaunchpad::OverlapPad);
  BoxComp->SetupAttachment(MeshComp);

  LaunchStrength = 1024;
  LaunchPitchAngle = 45;
}

void ALaunchpad::OverlapPad(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
  FRotator LaunchDirection = GetActorRotation();
  LaunchDirection.Pitch += LaunchPitchAngle;
  FVector LaunchVelocity = LaunchDirection.Vector() * LaunchStrength;

	AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
	if (MyCharacter != NULL) {
		MyCharacter->LaunchCharacter(LaunchVelocity, true, true);
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ActivateLaunchPadEffect, GetActorLocation());
	}
  else if (OtherComp && OtherComp->IsSimulatingPhysics())
  {
    OtherComp->AddImpulse(LaunchVelocity, NAME_None, true);
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ActivateLaunchPadEffect, GetActorLocation());
  }
}

