// Copyright 2020 Derek Fletcher. All Rights Reserved.


#include "FPSAIGuard.h"
#include "FPSGameMode.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
}

// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();
	
  OriginalRotation = GetActorRotation();
  GuardState = EAIState::Idle;

  PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnPawnSeen);
  PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPSAIGuard::OnHearNoise);

  if (bShouldPatrol) 
  {
    MoveToNextControlPoint();
  }
}

// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

  if (CurrentPatrolPoint) 
  {
    FVector Delta = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
    float DistanceToGoal = Delta.Size();

    if (DistanceToGoal < 100)
    {
      MoveToNextControlPoint();
    }
  }

}

void AFPSAIGuard::OnPawnSeen(APawn* SeenPawn)
{
  if (SeenPawn == nullptr) return;

    AFPSGameMode* const GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
		if (GM)
		{
			GM->CompleteMission(SeenPawn, false);
		}

    SetGuardState(EAIState::Alerted);

    AController* MyController = GetController();
    if (MyController != nullptr)
    {
      MyController->StopMovement();
    }
}

void AFPSAIGuard::OnHearNoise(APawn* NoiseInstigator, const FVector& Location, float Volume)
{
  if (GuardState == EAIState::Alerted) return;

  FVector Direction = Location - GetActorLocation();
  Direction.Normalize();

  FRotator NewLookAt = FRotationMatrix::MakeFromX(Direction).Rotator();
  NewLookAt.Pitch = NewLookAt.Roll = 0.f;

  SetActorRotation(NewLookAt);

  GetWorldTimerManager().ClearTimer(TimerHandle_ResetOrientation);
  GetWorldTimerManager().SetTimer(TimerHandle_ResetOrientation, this, &AFPSAIGuard::ResetOrientation, 3.f);

  SetGuardState(EAIState::Suspicious);

  AController* MyController = GetController();
  if (MyController != nullptr)
  {
    MyController->StopMovement();
  }
}

void AFPSAIGuard::ResetOrientation()
{
  if (GuardState == EAIState::Alerted) return;

  SetActorRotation(OriginalRotation);
  SetGuardState(EAIState::Idle);

  if (bShouldPatrol) 
  {
    MoveToNextControlPoint();
  }
}

void AFPSAIGuard::SetGuardState(EAIState NewState)
{
  if (GuardState == NewState) return;

  GuardState = NewState;
  OnRep_GuardState();
}

void AFPSAIGuard::MoveToNextControlPoint() 
{
  if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
  {
    CurrentPatrolPoint = FirstPatrolPoint;
  }
  else
  {
    CurrentPatrolPoint = SecondPatrolPoint;
  }

  UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
}

void AFPSAIGuard::OnRep_GuardState()
{
  OnStateChanged(GuardState);
}

void AFPSAIGuard::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
  Super::GetLifetimeReplicatedProps(OutLifetimeProps);

  DOREPLIFETIME(AFPSAIGuard, GuardState);
}