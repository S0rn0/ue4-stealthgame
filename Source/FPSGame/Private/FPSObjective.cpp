// Copyright 2020 Derek Fletcher. All Rights Reserved.

#include "FPSObjective.h"
#include "FPSCharacter.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

AFPSObjective::AFPSObjective()
{
	PrimaryActorTick.bCanEverTick = false;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	MeshComp->SetWorldScale3D(FVector(0.2f));
	MeshComp->SetMobility(EComponentMobility::Stationary);
	MeshComp->SetCollisionProfileName("NoCollision");

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetupAttachment(MeshComp);
	SphereComp->SetWorldScale3D(FVector(2.f));
	SphereComp->SetMobility(EComponentMobility::Stationary);
	SphereComp->SetCollisionProfileName("OverlapOnlyPawn");

  SetReplicates(true);
}

void AFPSObjective::BeginPlay()
{
	Super::BeginPlay();

	PlayEffects();
}

void AFPSObjective::PlayEffects()
{
	if (PickupFX != NULL) {
		UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation());
	}
}

void AFPSObjective::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

  if (HasAuthority())
  {
    AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
    if (MyCharacter != NULL) {
      PlayEffects();
      MyCharacter->bIsCarryingObjective = true;
      Destroy();
    }
  }
}

