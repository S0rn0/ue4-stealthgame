// Copyright 2020 Derek Fletcher. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlackHole.generated.h"

UCLASS()
class FPSGAME_API ABlackHole : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlackHole();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	/** The mesh representing the black hole */
	UPROPERTY(VisibleAnywhere, Category=Components)
	class UStaticMeshComponent* MeshComp;

	/** Handles overlaps to determine which actors are destroyed */
	UPROPERTY(VisibleAnywhere, Category=Components)
	class USphereComponent* InnerSphereComp;

	/** Handles overlaps to determine which actors are effected by forces */
	UPROPERTY(VisibleAnywhere, Category=Components)
	class USphereComponent* OuterSphereComp;

	UFUNCTION()
	void OverlapInnerSphere(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
