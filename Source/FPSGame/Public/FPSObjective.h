// Copyright 2020 Derek Fletcher. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSObjective.generated.h"

UCLASS()
class FPSGAME_API AFPSObjective : public AActor
{
	GENERATED_BODY()
	
public:	

	AFPSObjective();

protected:

	UPROPERTY(VisibleAnywhere, Category=Components)
	class UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category=Components)
	class USphereComponent* SphereComp;

	UPROPERTY(EditDefaultsOnly, Category=FX)
	class UParticleSystem* PickupFX;

	virtual void BeginPlay() override;

	void PlayEffects();

public:	

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};
