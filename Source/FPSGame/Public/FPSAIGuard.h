// Copyright 2020 Derek Fletcher. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSAIGuard.generated.h"

UENUM(BlueprintType)
enum class EAIState : uint8
{
  Idle,
  Suspicious,
  Alerted
};

UCLASS()
class FPSGAME_API AFPSAIGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSAIGuard();

  // Called every frame
	virtual void Tick(float DeltaTime) override;
  virtual void GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere, Category=Components)
  class UPawnSensingComponent* PawnSensingComp;

  UFUNCTION()
  void OnPawnSeen(APawn* SeenPawn);

  UFUNCTION()
  void OnHearNoise(APawn* NoiseInstigator, const FVector& Location, float Volume);

  UFUNCTION()
  void ResetOrientation();

  void SetGuardState(EAIState NewState);

  UFUNCTION(BlueprintImplementableEvent, Category=AI)
  void OnStateChanged(EAIState NewState);

  UPROPERTY(EditInstanceOnly, Category=AI)
  bool bShouldPatrol;

  UPROPERTY(EditInstanceOnly, Category=AI, meta = (EditConditions = "bShouldPatrol"))
  AActor* FirstPatrolPoint;

  UPROPERTY(EditInstanceOnly, Category=AI, meta = (EditConditions = "bShouldPatrol"))
  AActor* SecondPatrolPoint;

private:	

  UFUNCTION()
  void OnRep_GuardState();

  UPROPERTY(ReplicatedUsing=OnRep_GuardState)
  EAIState GuardState;

  FTimerHandle TimerHandle_ResetOrientation;
  FRotator OriginalRotation;
  AActor* CurrentPatrolPoint;

  void MoveToNextControlPoint();

};
