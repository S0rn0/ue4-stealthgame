// Copyright 2020 Derek Fletcher. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExtractionZone.generated.h"

UCLASS()
class FPSGAME_API AExtractionZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExtractionZone();

protected:

	UPROPERTY(VisibleAnywhere, Category=Components)
	class UBoxComponent* OverlapComp;

	UPROPERTY(VisibleAnywhere, Category=Components)
	class UDecalComponent* DecalComp;

	UFUNCTION()
	void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditDefaultsOnly, Category=Sounds)
	class USoundBase* ObjectiveMissingSound;
};
