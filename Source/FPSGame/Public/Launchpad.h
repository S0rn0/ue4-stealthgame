// Copyright 2020 Derek Fletcher. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Launchpad.generated.h"

UCLASS()
class FPSGAME_API ALaunchpad : public AActor
{
	GENERATED_BODY()
	
public:	
	ALaunchpad();

protected:

  UPROPERTY(EditInstanceOnly, Category=LaunchPad)
  float LaunchStrength;

  UPROPERTY(EditInstanceOnly, Category=LaunchPad)
  float LaunchPitchAngle;

  UPROPERTY(VisibleAnywhere, Category=Components)
  class UStaticMeshComponent* MeshComp;

  UPROPERTY(VisibleAnywhere, Category=Components)
  class UBoxComponent* BoxComp;

  UPROPERTY(EditDefaultsOnly, Category=LaunchPad)
  class UParticleSystem* ActivateLaunchPadEffect;

  UFUNCTION()
	void OverlapPad(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
